class CreatePostIds < ActiveRecord::Migration
  def change
    create_table :post_ids do |t|
      t.integer :title
      t.text :body

      t.timestamps
    end
  end
end
